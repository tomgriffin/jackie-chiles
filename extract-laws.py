from bs4 import BeautifulSoup
import json
from pathlib import Path
import sys

# Constants for file paths
SOURCE_DATA_PATH = Path("source-data")
PREDICATES_OF_VIOLENCE_FILENAME = "predicates_of_violence.txt"
LAW_HTML_FILENAME = "all-laws.html"
OUTPUT_DATA_PATH = "output_data.json"

def extract_body_text(body_container):
    """Extract text from a given body container."""
    paragraphs = body_container.find_all('p')
    return '\n\n'.join(p.get_text(strip=True) for p in paragraphs)

def parse_laws(html_content, predicates):
    """Parse law data from the given HTML content based on the specified predicates."""
    soup = BeautifulSoup(html_content, 'html.parser')
    laws_data = []

    for law in soup.find_all("div", class_="named-anchor clearfix section-6"):
        header_content = law.find('h1').get_text()
        citation, title = header_content.split('.', 1)
        last_updated = law.find('span', class_='date-display-single')['content'].split('T')[0]
        is_predicate_violence = citation.strip() in predicates
        body_content = law.find('div', class_='field field-name-body')
        body_text = extract_body_text(body_content) if body_content else ''

        laws_data.append({
            "citation": citation.strip(),
            "title": title.strip(),
            "last_updated": last_updated,
            "predicate_act_of_violence": is_predicate_violence,
            "body": body_text
        })
    
    return laws_data

def main():
    """Main function to extract and save law data."""
    html_file_path = SOURCE_DATA_PATH / LAW_HTML_FILENAME
    predicates_file_path = SOURCE_DATA_PATH / PREDICATES_OF_VIOLENCE_FILENAME
    output_path = SOURCE_DATA_PATH / OUTPUT_DATA_PATH

    try:
        with open(html_file_path, "r") as file:
            html_content = file.read()
    except FileNotFoundError:
        print(f"Error: HTML file not found at {html_file_path}")
        sys.exit(1)

    try:
        with open(predicates_file_path, "r") as file:
            predicates = file.read().splitlines()
    except FileNotFoundError:
        print(f"Error: Predicates file not found at {predicates_file_path}")
        sys.exit(1)

    laws_data = parse_laws(html_content, predicates)

    with open(output_path, 'w') as json_file:
        json.dump(laws_data, json_file, indent=4)

if __name__ == "__main__":
    main()
