# Jackie Chiles: Legal Counsel for the Innocent & Inappropriate

[![](http://img.youtube.com/vi/tqGu5bY2i6g/0.jpg)](http://www.youtube.com/watch?v=tqGu5bY2i6g "A Demo of Jackie Chiles")

Demo: http://www.youtube.com/watch?v=tqGu5bY2i6g

## You May Be Inappropriate, lewd, lascivious, and salacious - but that doesn't make you a criminal!

**I'm Jackie Chiles.** I'm an expert in the NJ Criminal Legal System.

I'm here to listen to what you did and then determine whether it's truly as bad as people are making it sound.

I'm an expert - but I don't rely on just my opinion to determine whether your antics amount to a crime or could be labeled a predicate to violence under NJ's domestic violence laws.

**I have actually memorized the jury instructions for each and every crime in NJ.** 

For example, let's say a guy tells me a story about a dispute he had at work with a guy named Bob Romano. Here's what he shared with me:

> I work in an office building in Morristown. It's got lots of conference rooms. I had the room reserved. This shouldn't have happened.
>
> I was in the conference room with one of the managers. This lady Jill. I was down on my knees, wrapping her naked body in saran wrap. That's when Bob Romano walked in on us.
>
> **I didn't want him to tell anyone what he saw!** I told Jill to finish the wrap on her own and I ran down the hallway after Bob. I told him that if he tells _anyone_ about what he saw then I would tell _everyone_ that he watches beastiality videos in his cubicle - and in fact he said one day that it was, _"the most amazing thing he's ever seen!"_
>
> I thought he'd shutup - but instead he's said that me threatening him like that is a crime. Am I in trouble?

### Could the story possibly align with any existing statutes? If so, which ones?

The closest crime to what this gentleman reported is 2C:13-5: Criminal Coercion. Now let's look at what happened as if I was a juror:

### So let's dissect that further - could this really result in a conviction under 2C:13-5?

Was he acting with the purpose of unlawfully restricting Bob Romano's freedom of action to engage or refrain from engaging in conduct?

**Yes he was.** 

He wanted to make sure Bob didn't go telling everyone about him, the saran wrap, and Jill's thighs.

I then have to look at whether the threat falls into any of the defined categories - and in this case, it does:

~~⛔️ Inflict bodily injury on anyone or commit any other offense;~~

~~⛔️ Accuse anyone of an offense;~~

✅ Expose any secret which would tend to subject any person to hatred, contempt or ridicule, or to impair his credit or business repute;

~~⛔️ Take or withhold action as an official, or cause an official to take or withhold action;~~

~~⛔️ Bring about or continue a strike, boycott or other collective action, except that such a threat shall not be deemed coercive when the restriction compelled is demanded in the course of negotiation for the benefit of the group in whose interest the actor acts;~~

~~⛔️ Testify or provide information or withhold testimony or information with respect to another's legal claim or defense; or~~

✅ Perform any other act which would not in itself substantially benefit the actor but which is calculated to substantially harm another person with respect to his health, safety, business, calling, career, financial condition, reputation or personal relationships.

On the surface there might be enough here for a conviction - but luckily I am also knowledgeable in affirmative defenses.

What are affirmative defenses? It's when the defendant did what he's being accused of, doesn't deny it, but brings circumstances to light which makes it a non-issue.

So I asked my guy...

**Did you believe this accusation you made about the beastiality videos was true?**

> Absolutely! It was true, Jackie!

**Did you really have plans to out Bob for being into animals? Or were you just trying to get his attention and shock him into keeping his mouth shut?**

> All I wanted to do was compell Bob Romano to behave in a way reasonably related to the circumstances. I wasn't actually going to share his secrets with everyone.
>
> I just wanted him to act normally! You don't walk in on someone, see them down under in the intimate act of wrapping a woman's body and saran wrap, and publish it in the next round of meeting.

**Do you think the State has any evidence that could convince a jury you did actually have plans to expose Bob's obsession with the animal kingdom?**

> No. I mean - it would be uncomfortable even talking about that kind of stuff. That's the kind of stuff that happens in public schools.

**Good answers!** A jury with this information would have to find you not guilty. Even though you technically did it, your affirmative defense will likely get you off the hook :)