import json
import requests
from PyPDF2 import PdfReader, PdfMerger



# Path to the JSON file
json_file_path = 'laws_w_guidance.json'

# Directory to save the downloaded PDFs
download_dir = 'jury_guidance'

# Initialize the PDF merger
merger = PdfMerger()

def download_pdfs(url_list):
    valid_pdf_paths = []  # List to store paths of successfully downloaded PDFs
    for index, url in enumerate(url_list):
        try:
            response = requests.get(url)
            if response.status_code != 200:
                print(f"Failed to download PDF from {url} - Status code: {response.status_code}")
                continue
            pdf_path = f'{download_dir}document_{index}.pdf'
            with open(pdf_path, 'wb') as f:
                f.write(response.content)
            # Attempt to read the PDF to verify its validity
            try:
                with open(pdf_path, "rb") as f:
                    PdfReader(f)
                valid_pdf_paths.append(pdf_path)
            except Exception as e:  # Catching a general exception from PyPDF2
                print(f"Downloaded file is not a valid PDF: {pdf_path}. Error: {e}")
        except requests.exceptions.RequestException as e:
            print(f"Failed to download {url}: {e}")
    return valid_pdf_paths


def merge_pdfs(pdf_paths, output_path):
    for pdf_path in pdf_paths:
        try:
            merger.append(pdf_path)
        except PyPDF2.errors.PdfReadError as e:
            print(f"Error reading PDF {pdf_path}: {e}")
    merger.write(output_path)
    merger.close()

def main():
    # Load the JSON data
    with open(json_file_path, 'r') as file:
        data = json.load(file)

    # Extract the URLs
    urls = [guidance['url'] for item in data if 'jury_guidance' in item for guidance in item['jury_guidance']]

    # Download the PDFs and get the list of valid PDF paths
    valid_pdf_paths = download_pdfs(urls)

    # # Merge the valid PDFs
    # output_pdf_path = 'merged_document.pdf'
    # merge_pdfs(valid_pdf_paths, output_pdf_path)
    # print(f'Merged PDF saved to {output_pdf_path}')

if __name__ == '__main__':
    main()
