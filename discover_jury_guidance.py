import json
from pathlib import Path
from serpapi import GoogleSearch

# Constants
SOURCE_DATA_PATH = Path("source-data")
LAW_JSON_FILENAME = "laws.json"
OUTPUT_FILENAME = "laws_w_guidance.json"
API_KEY = "9d2d88a67e4a8b8d47733a6e48fa5c18cd354a3280ea9337d65956d0b63446c0"

def enrich_json_file(source_path, source_filename, output_filename, api_key):
    # Construct the full path to the source file
    source_file_path = source_path / source_filename
    
    # Load the JSON data
    with open(source_file_path, 'r') as file:
        data = json.load(file)

    # Iterate over each item and enrich it if predicate_act_of_violence is true
    for item in data:
        if item.get('predicate_act_of_violence'):
            params = {
                "api_key": api_key,
                "engine": "google",
                "q": f"filetype:pdf AND site:njcourts.gov AND inurl:sites/default/files/charges AND \"{item['citation']}*\" AND \"{item['title']}\"",
                "location": "Austin, Texas, United States",
                "google_domain": "google.com",
                "gl": "us",
                "hl": "en",
                "filter": "0"
            }

            search = GoogleSearch(params)
            results = search.get_dict()
            
            # Check if there are organic results
            if 'organic_results' in results and results['organic_results']:
                item['jury_guidance'] = []
                for result in results['organic_results']:
                    item['jury_guidance'].append({
                        'title': result.get('title', ''),
                        'snippet': result.get('snippet', ''),
                        'url': result.get('link', '')
                    })

    # Save the enriched data back to the specified output file
    with open(output_filename, 'w') as file:
        json.dump(data, file, indent=4)

# Use the constants in the function call
enrich_json_file(SOURCE_DATA_PATH, LAW_JSON_FILENAME, OUTPUT_FILENAME, API_KEY)
